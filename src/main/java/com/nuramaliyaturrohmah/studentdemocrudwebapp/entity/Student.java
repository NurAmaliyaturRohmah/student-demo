/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nuramaliyaturrohmah.studentdemocrudwebapp.entity;

/**
 *@222112268_Nur Amaliyatur Rohmah
 * 
 * Kelas Student merupakan kelas entitas yang berisi id, first name, last name, birthdate, created on dan updateon.
 * Didalma kelas student terdapat hibernate dimana saat aplikasi dijalankan maka hibernate akan membuat tabel 
 * “tbl_student” dengan konfigurasi yang telah diatur melalui kelas entitas Student.
 * 
 */
import jakarta.persistence.*;
//hibernate
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import java.util.Date;
import lombok.Setter;
import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
       
 @Setter
 @Getter
 @AllArgsConstructor
 @NoArgsConstructor
 @Builder
 @Entity
 @Table(name = "tbl_student")
public class Student {
     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     private Long id;
     @Column(nullable = false)
     private String firstName;
     @Column(nullable = false)
     private String lastName;
     @Column(nullable = false)
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
     private Date birthDate;
     @Column(nullable = false)
     @CreationTimestamp
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
     private Date createdOn;
     @UpdateTimestamp
     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
     private Date updatedOn;   
}
