/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *@222112268_Nur Amaliyatur Rohmah
 * Kelas StudentMapper merupakan kelas yang digunakan untuk merubah Student Entity ke StudentDTO dan sebaliknya.
 * Dalam kelas StudentMapper terdapat method static dan method builder. Dimana method static digunakan agar tidak perlu 
 * membuat objek baru dan bisa langsung diakses, sedangkan method builder berguna untuk mengambil dari student entity.
 */
package com.nuramaliyaturrohmah.studentdemocrudwebapp.mapper;
import com.nuramaliyaturrohmah.studentdemocrudwebapp.dto.StudentDto;
import com.nuramaliyaturrohmah.studentdemocrudwebapp.entity.Student;


public class StudentMapper {
    //map Student entity to Student Dto
    public static StudentDto mapToStudentDto(Student student){
        //Membuat dto dengan builder pattern (inject dari lombok)
        StudentDto studentDto = StudentDto.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .birthDate(student.getBirthDate())
                .createdOn(student.getCreatedOn())
                .updatedOn(student.getUpdatedOn())
                .build();
        return studentDto;
    }
    
    //map Student Dto ke Student Entity
    public static Student mapToStudent(StudentDto studentDto){
        Student student = Student.builder()
                .id(studentDto.getId())
                .firstName(studentDto.getFirstName())
                .lastName(studentDto.getLastName())
                .birthDate(studentDto.getBirthDate())
                .createdOn(studentDto.getCreatedOn())
                .updatedOn(studentDto.getUpdatedOn())
                .build();
        return student;
    }
}
