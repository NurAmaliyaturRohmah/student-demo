/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nuramaliyaturrohmah.studentdemocrudwebapp.service;

/**
 *
 * @author Acer
 */

import com.nuramaliyaturrohmah.studentdemocrudwebapp.dto.StudentDto;
import java.util.List;

public interface StudentService {
    public List<StudentDto> ambilDaftarStudent();
    public void perbaruiDataStudent(StudentDto studentDto);
    public void hapusDataStudent(Long studentId);
    public void simpanDataStudent(StudentDto studentDto);

    public StudentDto cariById(Long id);
}
